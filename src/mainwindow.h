#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "third-party/cutemime/cutemime.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private slots:
        void on_lw_apps_currentItemChanged(QListWidgetItem *current);
        void on_btn_icon_clicked();
        void on_le_iconname_editingFinished();
        void on_actionDelete_triggered();
        void on_actionSave_triggered();
        void on_actionExit_triggered();
        void on_btn_search_clicked();
        void on_actionNew_triggered();

        void on_close_clicked();

        void on_lw_apps_customContextMenuRequested(const QPoint &pos);

private:
        Ui::MainWindow *ui;
        void populateApps();
        void loadCategories(const QStringList &categories);

        QMap<QString, Desktop> appMap;
        QMenu *menu;
};

#endif // MAINWINDOW_H
