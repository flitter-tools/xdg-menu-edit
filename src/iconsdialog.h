#ifndef ICONSDIALOG_H
#define ICONSDIALOG_H

#include <QDialog>
#include <QDir>
#include <QFuture>

namespace Ui {
class IconsDialog;
}

class IconsDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit IconsDialog(QWidget *parent = nullptr);
        ~IconsDialog();

        QString getIcon() const;

    private slots:
        void on_buttonBox_accepted();
        void on_buttonBox_rejected();
        void on_le_search_textChanged(const QString &arg1);

    private:
        Ui::IconsDialog *ui;

        void populateIcons(const QString &icons);

        QString m_icon;
};

#endif // ICONSDIALOG_H
