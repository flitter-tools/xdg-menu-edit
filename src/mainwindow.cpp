#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDirIterator>
#include <QFileInfo>
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>
#include <QMap>
#include <QRandomGenerator>
#include <QMenu>
#include <QAction>
#include <QStandardPaths>

#include "iconsdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->icon->setPixmap(QIcon::fromTheme("gnome-gmenu").pixmap(45));

    populateApps();

    for (int i = 0; i < ui->lw_categories->count(); ++i)
    {
        QListWidgetItem *item = ui->lw_categories->item(i);
        item->setCheckState(Qt::Unchecked);
    }

    menu = new QMenu;
    menu->addAction(ui->actionNew);
    menu->addAction(ui->actionSave);
    menu->addAction(ui->actionDelete);
}

MainWindow::~MainWindow()
{
    delete menu;
    delete ui;
}

void MainWindow::populateApps()
{
    ui->lw_apps->clear();

    for (Desktop desktop : CuteMime().getListDesktopFiles("", false))
    {
        QListWidgetItem *item = new QListWidgetItem(ui->lw_apps);
        item->setText(desktop.name);
        item->setIcon(desktop.icon);
        appMap[desktop.filePath] = desktop;
        item->setData(Qt::UserRole, desktop.filePath);
    }
}

void MainWindow::loadCategories(const QStringList &cat)
{
    for (int i = 0; i < ui->lw_categories->count(); ++i)
    {
        QListWidgetItem *item = ui->lw_categories->item(i);
        item->setCheckState(Qt::Unchecked);
    }

    for (int i = 0; i < ui->lw_categories->count(); ++i)
    {
        QListWidgetItem *item = ui->lw_categories->item(i);
        if (cat.contains(item->text()))
            item->setCheckState(Qt::Checked);
    }
}

void MainWindow::on_lw_apps_currentItemChanged(QListWidgetItem *current)
{
    if (current == nullptr)
        return;

    Desktop desktop = appMap[current->data(Qt::UserRole).toString()];

    ui->le_name->setText(desktop.name);
    ui->le_exec->setText(desktop.exec);
    ui->le_iconname->setText(desktop.iconRealName);
    ui->btn_icon->setIcon(desktop.icon);
    ui->cb_nodisplay->setChecked(desktop.noDisplay);

    QFileInfo info(desktop.filePath);
    ui->l_system->setHidden(info.isWritable());
    ui->actionDelete->setEnabled(info.isWritable());

    loadCategories(desktop.categories);

    // when set text to lineedit cursor set to last position by default
    // I need it to be in first position
    // is there a better way doing this?
    ui->le_iconname->setCursorPosition(0);
    ui->le_exec->setCursorPosition(0);
    ui->le_name->setCursorPosition(0);
}

void MainWindow::on_btn_icon_clicked()
{
    // DO NOT allocate memory for this class in constructor, app will launch couple seconds instead of instantly
    IconsDialog dialog;
    if (dialog.exec() == QDialog::Accepted)
    {
        QIcon icon = QIcon::fromTheme(dialog.getIcon());
        ui->btn_icon->setIcon(icon);
        ui->le_iconname->setText(dialog.getIcon());
    }
}

void MainWindow::on_le_iconname_editingFinished()
{
    ui->btn_icon->setIcon(QIcon::fromTheme(ui->le_iconname->text(), QIcon::fromTheme("action-unavailable")));
}

void MainWindow::on_actionDelete_triggered()
{
    QListWidgetItem *item = ui->lw_apps->currentItem();
    QMessageBox msg;
    msg.setText("Do you realy want to delete " + item->text());
    msg.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    int result = msg.exec();

    if (result == QMessageBox::Ok)
    {
        QFile::remove(item->data(Qt::UserRole).toString());
        delete item;
    }
}

void MainWindow::on_actionSave_triggered()
{
    // todo:
    // update if we're copy an protected file

    QStringList checked;

    for (int i = 0; i < ui->lw_categories->count(); ++i)
    {
        QListWidgetItem *item = ui->lw_categories->item(i);
        if (item->checkState() == Qt::Checked)
            checked.push_back(item->text());
    }

    QListWidgetItem *item = ui->lw_apps->currentItem();
    item->setIcon(ui->btn_icon->icon());
    item->setText(ui->le_name->text());

    QString path = item->data(Qt::UserRole).toString();
    QFile file(path);
    if (!file.isWritable())
    {
        QString applications = QStandardPaths::standardLocations(QStandardPaths::ApplicationsLocation).first();
        path = applications + "/" + QFileInfo(path).baseName() + "." + QFileInfo(path).suffix();
        file.copy(path);
    }

    Desktop desktop;
    desktop.exec = ui->le_exec->text();
    desktop.name = ui->le_name->text();
    desktop.tryExec = ui->le_exec->text();
    desktop.filePath = path;
    desktop.terminal = false;
    desktop.noDisplay = ui->cb_nodisplay->isChecked();
    desktop.categories = checked;
    desktop.iconRealName = ui->le_iconname->text();

    CuteMime().write(desktop);
}

void MainWindow::on_actionExit_triggered()
{
    qApp->quit();
}

void MainWindow::on_btn_search_clicked()
{
    const QString &fileName = QFileDialog::getOpenFileName(this,
        tr("Open Image"), QDir::homePath(), tr("Image Files (*.png *.jpg *.bmp *.xpm)"));

    if (!fileName.isEmpty())
    {
        ui->le_iconname->setText(fileName);
        on_le_iconname_editingFinished();
    }
}

void MainWindow::on_actionNew_triggered()
{
    QString defaultname = "untitled";
    QString applications = QStandardPaths::standardLocations(QStandardPaths::ApplicationsLocation).first();
    QFile file(applications + "/untitled.desktop");
    if (file.exists())
    {
        int rng = QRandomGenerator::global()->bounded(INT_MAX);
        defaultname += QString::number(rng);
    }

    QInputDialog dialog(this);
    dialog.setLabelText(tr("Name:"));
    dialog.setTextValue(defaultname);
    if (dialog.exec() == QDialog::Rejected)
        return;

    const QString &name = dialog.textValue();

    Desktop desktop;
    desktop.exec = "";
    desktop.name = name;
    desktop.tryExec = "";
    desktop.filePath = applications + "/" + name + ".desktop";
    desktop.terminal = false;
    desktop.noDisplay = false;
    desktop.iconRealName = "";

    CuteMime().write(desktop);

    // optimize update?
    populateApps();

    for (int i = 0; i < ui->lw_apps->count(); ++i)
    {
        QListWidgetItem *item = ui->lw_apps->item(i);
        if (item->text() == name)
        {
            ui->lw_apps->scrollToItem(item);
            item->setSelected(true);
            ui->lw_apps->setCurrentItem(item);
            break;
        }
    }
}

void MainWindow::on_close_clicked()
{
    on_actionExit_triggered();
}

void MainWindow::on_lw_apps_customContextMenuRequested(const QPoint &pos)
{
    QPoint globalPos = ui->lw_apps->mapToGlobal(pos);
    menu->exec(globalPos);
}
