#-------------------------------------------------
#
# Project created by QtCreator 2018-09-25T10:32:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = xdg-menu-edit
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(third-party/cutemime/CuteMime.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
        iconsdialog.cpp

HEADERS  += mainwindow.h \
    iconsdialog.h

FORMS    += mainwindow.ui \
    iconsdialog.ui

bin.path   = /usr/bin
bin.files   = xdg-menu-edit

shortcut.path = /usr/share/applications
shortcut.files = xdg-menu-edit.desktop

INSTALLS += bin shortcut
