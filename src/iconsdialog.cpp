#include "iconsdialog.h"
#include "ui_iconsdialog.h"

#include <QIcon>
#include <QDirIterator>
#include <QStringList>
#include <QFileInfo>
#include <QFileDialog>
#include <QtConcurrent/QtConcurrentRun>

IconsDialog::IconsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IconsDialog)
{
    ui->setupUi(this);

    QString path;

    for (const QString &item : QIcon::themeSearchPaths())
    {
        QDir dir(item + "/" + QIcon::themeName() + "/48x48/apps");
        if (dir.exists())
        {
            path = dir.path();
            break;
        }
    }

    QtConcurrent::run(this, &IconsDialog::populateIcons, path);
}

IconsDialog::~IconsDialog()
{
    delete ui;
}

void IconsDialog::populateIcons(const QString &dir)
{
    QDirIterator it(dir, QStringList() << "*.svg" << "*.png");

    while(it.hasNext())
    {
        QFile file(it.next());

        // we cant just use basename here, because real icons names more complicated
        const QStringList &realName = file.fileName().split("/");
        const QStringList &removeExt = realName.last().split(".");
        QString baseName = "";

        if (removeExt.count() > 2)
        {
            for (int i = 0; i < removeExt.count()-1; ++i)
            {
                if (i == removeExt.count()-2)
                    baseName += removeExt[i];
                else
                    baseName += removeExt[i] + ".";
            }
        }
        else
        {
            baseName = removeExt.first();
        }

        QListWidgetItem *item = new QListWidgetItem(QIcon::fromTheme(file.fileName()), baseName);
        ui->lw_icons->addItem(item);
    }

    ui->l_loading->setHidden(true);
}

QString IconsDialog::getIcon() const
{
    return m_icon;
}

void IconsDialog::on_buttonBox_accepted()
{
    QListWidgetItem *item = ui->lw_icons->currentItem();
    if (item)
        m_icon =  item->text();

    this->accept();
}

void IconsDialog::on_buttonBox_rejected()
{
    this->reject();
}

void IconsDialog::on_le_search_textChanged(const QString &arg1)
{
    for (int i = 0; i < ui->lw_icons->count(); ++i)
        ui->lw_icons->item(i)->setHidden(false);

    if (arg1.isEmpty())
        return;

    for (int i = 0; i < ui->lw_icons->count(); ++i)
    {
        if (!ui->lw_icons->item(i)->text().contains(arg1))
            ui->lw_icons->item(i)->setHidden(true);
    }
}
