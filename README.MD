# XDG Menu Edit

Easy manage your XDG menu (found in /usr/share/applications or ~/.local/share/apllications).

This program is part of [Flitter](https://gitlab.com/flitter-tools), all informations can be found in [this repository](https://gitlab.com/flitter-tools/General).

### How to use

Click in apps filed to open context menu with options available such as New, Save and Delete.

### Screenshots

![mainwindow](screenshots/mainwindow.png)

### Shortcuts


```Ctrl+S -- Save current entry (if it's system it will be saved to``` [XDG_DATA_HOME](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html#variables) ```/applications)```

```Del -- Delete current entry (if it's not system)```

```Ctrl+Q -- Quit```

### License

See LICENSE file in repository root.
